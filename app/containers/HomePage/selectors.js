/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectHome = state => state.home || initialState;

const makeSelectUsername = () =>
  createSelector(
    selectHome,
    homeState => homeState.username,
  );
const makeSelectData = () =>
  createSelector(
    selectHome,
    homeState => homeState.data,
  );
const makeSelectSearch = () =>
  createSelector(
    selectHome,
    homeState => homeState.search,
  );
const makeSelectButton = () =>
  createSelector(
    selectHome,
    homeState => homeState.button,
  );
export {
  selectHome,
  makeSelectUsername,
  makeSelectData,
  makeSelectSearch,
  makeSelectButton,
};
