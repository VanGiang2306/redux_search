/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React, { useEffect, memo, useState, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import {
  makeSelectRepos,
  makeSelectLoading,
  makeSelectError,
} from 'containers/App/selectors';
import H2 from 'components/H2';
import ReposList from 'components/ReposList';
import AtPrefix from './AtPrefix';
import CenteredSection from './CenteredSection';
import Form from './Form';
import Input from './Input';
import Section from './Section';
import messages from './messages';
import { loadRepos } from '../App/actions';
import { changeSearch, changeUsername, getData, changeButton } from './actions';
import {
  makeSelectData,
  makeSelectUsername,
  makeSelectSearch,
  makeSelectButton,
} from './selectors';
import reducer from './reducer';
import saga from './saga';

const key = 'home';

export function HomePage({
  username,
  loading,
  error,
  repos,
  onSubmitForm,
  onChangeUsername,
  data, // []
  getData,
  changeSearch,
  changeButton,
}) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  const [search, setSearch] = useState('');
  const [dataRep, setDataRep] = useState([]);
  const [edit, setEdit] = useState('');

  useEffect(() => {
    // When initial state username is not null, submit the form to load repos
    if (username && username.trim().length > 0) onSubmitForm();
  }, []);

  useEffect(() => {
    setDataRep(data);
  }, [data]);

  console.log('data', dataRep);

  useEffect(() => {
    getData('HungBay');
  }, []);

  const reposListProps = {
    loading,
    error,
    repos,
  };

  const handleChangeSearch = useCallback(
    e => {
      setSearch(e.target.value);
    },
    [search],
  );
  // const hanldeChangeClick = () => {
  //   changeSearch(search);
  // };

  const onDelete = id => {
    const copyData = [...dataRep];
    console.log(id);
    const index = copyData.findIndex(el => {
      if (el.id === id) {
        return el;
      }
    });
    const newData = copyData.splice(index, 1);
    console.log(1, newData);
    setDataRep(copyData);
  };

  const onUpdate = el => {
    console.log(1, el);
  };

  const handleChange = e => {
    const { value, name } = e.target;
    const copyData = [...dataRep];
    const foundData = copyData.filter(i =>
      i.name.toLowerCase().includes(e.target.value.toLowerCase().trim()),
    );
    if (e.target.value !== '') {
      setDataRep(foundData);
    } else {
      setDataRep(data);
    }
  };
  const handleEdit = el => {
    setDataRep(el);
  };

  // const handleChangeButton = () => {};
  return (
    <article>
      <Helmet>
        <title>Home Page</title>
        <meta
          name="description"
          content="A React.js Boilerplate application homepage"
        />
      </Helmet>
      <div>
        {/* <input type="text" onChange={handleChangeSearch} />
        <button onClick={hanldeChangeClick}>search</button> */}
        <input type="text" onChange={handleChange} />
        <input type="text" onChange={handleEdit} value={el.name} />
        {dataRep.map(el => {
          return (
            <>
              <tr>
                <td>{el.name}</td>
                <button onClick={() => onDelete(el.id)}>delete</button>
                <button onClick={() => onUpdate(el)}>edit</button>
              </tr>
            </>
          );
        })}
      </div>
    </article>
  );
}

HomePage.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  repos: PropTypes.oneOfType([PropTypes.array, PropTypes.bool]),
  onSubmitForm: PropTypes.func,
  username: PropTypes.string,
  onChangeUsername: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  repos: makeSelectRepos(),
  username: makeSelectUsername(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
  data: makeSelectData(),
  search: makeSelectSearch(),
  button: makeSelectButton(),
});

export function mapDispatchToProps(dispatch) {
  return {
    onChangeUsername: evt => dispatch(changeUsername(evt.target.value)),
    onSubmitForm: evt => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(loadRepos());
    },
    getData: function(username) {
      dispatch(getData(username));
    },
    changeSearch: s => dispatch(changeSearch(s)),
    changeButton: b => dispatch(changeButton(b)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(HomePage);
