/**
 * Gets the repositories of the user from Github
 */

import { call, fork, put, select, takeLatest } from 'redux-saga/effects';
import { LOAD_REPOS } from 'containers/App/constants';
import { reposLoaded, repoLoadingError } from 'containers/App/actions';

import request from 'utils/request';
import { makeSelectUsername } from 'containers/HomePage/selectors';
import { CHANGE_SEARCH, GET_DATA, CHANGE_BUTTON } from './constants';
import { getDataSuccess } from './actions';

/**
 * Github repos request/response handler
 */
export function* getRepos() {
  // Select username from store
  const username = yield select(makeSelectUsername());
  const requestURL = `https://api.github.com/users/${username}/repos?type=all&sort=updated`;

  try {
    // Call our request helper (see 'utils/request')
    const repos = yield call(request, requestURL);
    yield put(reposLoaded(repos, username));
  } catch (err) {
    yield put(repoLoadingError(err));
  }
}
function* getDataSaga(action) {
  const { username } = action;
  const requestURL = `https://api.github.com/users/${username}/repos?type=all&sort=updated`;

  try {
    // Call our request helper (see 'utils/request')
    const repos = yield call(request, requestURL);
    console.log(action);
    yield put(getDataSuccess(repos));
  } catch (err) {
    // yield put(repoLoadingError(err));
  }
}
export function* getSearch(action) {
  const { search } = action;
  try {
    const requestURL = `https://api.github.com/users/HungBay/repos?type=all&sort=updated`;
    // Call our request helper (see 'utils/request')
    const repos = yield call(request, requestURL);
    if (search !== '') {
      const newData = repos.filter(el => el.name === search);
      yield put(getDataSuccess(newData));
    } else {
      yield put(getDataSuccess(repos));
    }
  } catch (err) {
    console.log(err);
  }
}
export function* getButton(action) {
  const { button } = action;
  console.log(2, button);
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* githubData() {
  // Watches for LOAD_REPOS actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield takeLatest(LOAD_REPOS, getRepos);
  yield takeLatest(GET_DATA, getDataSaga);
  yield takeLatest(CHANGE_SEARCH, getSearch);
  yield takeLatest(CHANGE_BUTTON, getButton);
}
