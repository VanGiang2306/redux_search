/*
 * Home Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import {
  CHANGE_USERNAME,
  GET_DATA,
  GET_DATA_SUCCESS,
  GET_DATA_FAILED,
  CHANGE_SEARCH,
  CHANGE_BUTTON,
  DELETE_DATA,
} from './constants';

/**
 * Changes the input field of the form
 *
 * @param  {string} username The new text of the input field
 *
 * @return {object} An action object with a type of CHANGE_USERNAME
 */
export function changeUsername(username) {
  return {
    type: CHANGE_USERNAME,
    username,
  };
}

export function getData(username) {
  return {
    type: GET_DATA,
    username,
  };
}

export function getDataSuccess(data) {
  return {
    type: GET_DATA_SUCCESS,
    data,
  };
}
export function getDataFailed(error) {
  return {
    type: GET_DATA_FAILED,
    error,
  };
}
export function changeSearch(search) {
  return {
    type: CHANGE_SEARCH,
    search,
  };
}
export function changeButton(button) {
  return {
    type: CHANGE_BUTTON,
    button,
  };
}
export function deleteData(data) {
  return {
    type: DELETE_DATA,
    data,
  };
}
