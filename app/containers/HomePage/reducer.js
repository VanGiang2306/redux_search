/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can
 * update our application state. To add a new action,
 * add it to the switch statement in the reducer function
 *
 */

import produce from 'immer';
import {
  CHANGE_SEARCH,
  CHANGE_USERNAME,
  GET_DATA_FAILED,
  GET_DATA_SUCCESS,
  CHANGE_BUTTON,
} from './constants';

// The initial state of the App
export const initialState = {
  username: '',
  data: [],
  error: [],
};

/* eslint-disable default-case, no-param-reassign */
const homeReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case CHANGE_USERNAME:
        // Delete prefixed '@' from the github username
        draft.username = action.username.replace(/@/gi, '');
        break;
      case GET_DATA_SUCCESS:
        draft.data = action.data;
        break;
      case GET_DATA_FAILED:
        draft.error = action.error;
        break;
      case CHANGE_SEARCH:
        draft.search = action.search;
        break;
      case CHANGE_BUTTON:
        draft.button = action.button;
        break;
    }
  });

export default homeReducer;
